<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 */
?>

	</div><!-- #main -->
</div><!-- #page -->


	<footer id="footer" role="contentinfo">
		
		<div class="footer-wrapper">
			<nav role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'secondary' ) ); ?>
			</nav><!-- #access -->
			<section>
				<span class="copyright">&copy; <?php echo date("Y"); ?> The Rockefeller Group International All Rights Reserved</span>
				<ul class="links">
					<li><a href="/terms/" title="Terms">Terms</a></li>
					<li><a href="/privacy-policy/" title="Policy">Privacy Policy</a></li>
				</ul>
				<span class="credit"><a href="http://nicasiodesign.com/" target="_blank">Website by Nicasio LLC</a></span>	
			</section>	
		</div>	
	</footer><!-- #footer -->

<?php wp_footer(); ?>

</body>
</html>
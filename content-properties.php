<?php
/**
 * The template used for displaying page content in archive-properties.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('teaser'); ?>>
	
	
	<div class="entry-content entry-body">
		<div class="col col_1">

			<?php get_template_part('content','properties-featured-image'); ?>
		</div>
		
		<div class="col col_2">

			<?php get_template_part('content','properties-misc-details'); ?>
			
			<footer class="read-more more-info">
				<span class="read-more"><a href="<?php the_permalink(); ?>">More Info</a></span>
			</footer>	


		</div>	
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
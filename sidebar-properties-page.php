<?php 
 /**
 * The template used for displaying sidebar 2 (Properties Page sidebar)
 */

		if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
			<div id="secondary" class="widget-area" role="complementary">
				<ul class="sidebar">
					<?php dynamic_sidebar( 'sidebar-2' ); ?>
				</ul>
			</div><!-- #secondary .widget-area -->
				
		<?php endif; ?>
<?php
/**
 * The template used for displaying carousel on page-homepage.php
 */


	$items = get_field('homepage_rotating_images'); 
	

	if($items) { ?>
	<section class="region-top">
		<ul id="featured-images" class="entry-images">

			<?php
			foreach($items as $i) { ?>
				<li class="slide" id="hpi-<?= $i->ID ?>">
					
					<?php

					if (has_post_thumbnail($i->ID)) {
						echo '<div class="item-img entry-media">'; ?>
						<a href="<?php echo get_permalink($i->ID); ?>" title="<?php echo $i->post_title; ?>" ><?php echo get_the_post_thumbnail($i->ID, 'property-featured' ); ?></a>
						<?php
						echo '</div>';
					}
					?>
		
				</li>
			<?php } ?>

		</ul>

		
		<ul id="featured-text">

			<?php


                        // Using array reverse here to get text items out in reverse order.
                        // Images are displayed FIFO (First in First Out), Text is displayed
                        // LIFO (Last in First Out).  Reversing one fixes it for initial load
			$items2 = array_reverse(get_field('homepage_rotating_images'));
	
			foreach($items2 as $i) { ?>
				<li class="slide" id="hpt-<?= $i->ID ?>">
					<div class="item-text">

				
						<h2><?php echo $i->post_title; ?></h2>
						<?php
		
						if(get_field('acf_excerpt',$i->ID )){
							$excerpt =  get_field('prop_excerpt',$i->ID);
						} else {	
							$content = $content = wp_trim_words( $i->post_content, 40, '' );  
						}	
							
						// $inbtw = '...';
						// if(strlen(strip_tags($excerpt)) != strlen($excerpt)) {
						// 	$inbtw = ' ';
						// } else {
						// 	$inbtw = '...';
						// }
						$read_more = '<span class="read-more"><a href="/properties/'. $i->post_name . '">Read More</a></span>';
					
						if($excerpt ){
							echo $excerpt . $read_more;
						} else {
							echo $content . $read_more;
						}

						?>
					</div>
				
		
				</li>
			<?php } ?>

		</ul>
		 <div id="carousel-pager" class="pagination"></div>
	</section>	
<?php }  ?> 

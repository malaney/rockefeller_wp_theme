<?php
/**
 * Template Name: Properties Page
 * The template used for displaying different property types
 */

get_header(); ?>


	<div class="region-content">
		<div id="primary">
			<div id="content" role="main">
				
				<?php

				$args = array(
					'hide_empty'    => false,
				);
				$taxonomy = 'property_categories';
				$tax_terms = get_terms($taxonomy, $args);

				foreach( $tax_terms as $tax_term ) {
						$tax_name = is_tax( $taxonomy, $tax_term->name );
						if($tax_name) { ?>

							<h1 class="entry-title">
								<?php echo  bloginfo( 'name' ) . ' ' .  $tax_term->name . ' Properties'; ?> 
							</h1>
						<?php
						}
						$term = has_term( $tax_term->slug, $taxonomy);

						if($term) { 
							$args = array(
								'post_type' => 'properties',
								'paged' => $paged,
								'tax_query' => array(
									array(
										'taxonomy' => 'property_categories',
										'field' => 'slug',
										'terms' => $tax_term->slug
									)
								)
							);

							$wp_query = new WP_Query( $args );


							// The Loop
							while ( $wp_query->have_posts() ) :
								$wp_query->the_post(); 
								get_template_part( 'content', 'properties' );
								
							endwhile;	
							
						wp_reset_postdata();	
						} 
				
				}	?>

				<?php get_template_part( 'nav-pager' ); ?>

				

				
			</div><!-- #content -->
		</div><!-- #primary -->

<?php get_sidebar('properties'); ?>
</div>

<?php get_footer(); ?>
<?php

		if( get_field('prop_information') ): 
		 	while( has_sub_field('prop_information') ): 
		 		if( get_sub_field('prop_agent') ): 
		 			$agents = get_sub_field('prop_agent'); 
 					if($agents) {
 						echo '<footer class="entry-contact-info">';
	 					foreach($agents as $agent) {
	 						if(get_field('agent_name',$agent->ID)) {
	 							$name = get_field('agent_name',$agent->ID);
	 						} else {
	 						 	$name = $agent->post_title;
	 						}
							$email = get_field('agent_email', $agent->ID);
							$phone = get_field('agent_phone', $agent->ID);
							if(!($name == '' && $email == '' && $phone == '')) {
								if($email && $phone) {
									echo 'Email <a href="mailto:' . $email .'">' . $name . '</a> for more info or call ' . $phone;
								} elseif($email && !$phone ) {					
									echo 'Email <a href="mailto:' . $email .'">' . $name . '</a> for more info';
								} elseif($phone && !$email) {
									echo 'Call ' . $name . ' at ' , $phone;
								}
							}
					 	}
					 	echo '</footer>';
					} 	
 				?>
 			<?php 
		 		endif;
		 	endwhile;
		endif;
?>
<?php 
/**
 * The template used for specific property's image gallery in content-single-properties.php
 */

	$items = get_field('property_image_gallery'); 


	if($items) { ?>
		<div class="gallery">
			<?php $count = 1; ?>
				<?php
				echo '<ul class="entry-images entry-media">';
				foreach($items as $i) { ?>

							<li><?php echo wp_get_attachment_link( $i['id'] ,'property-small-thumb' ); ?></li>
							<?php if( (5 % $count) == 0 && $count != 1 ) { ?>
									</ul>
									<?php
									if(count($items) > 5) { ?>
										<ul class="entry-images entry-media">
										<?php $count = 0; ?>	
									<?php } ?>		
							<?php } ?>	
							<?php if($count == count($items)) { ?>
									</ul>
							<?php } ?>
				
						
						<?php $count++; ?>

				<?php } ?>

			</ul>
		</div>

	<?php }  ?> 
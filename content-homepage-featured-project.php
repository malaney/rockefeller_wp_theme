 <?php
/**
 * The template used for displaying featured project on page-homepage.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('teaser post'); ?>>
	<?php

	if(get_field('prop_information')) {
		while( has_sub_field('prop_information') ):
			if( get_sub_field('prop_featured_project_image')) {
				$img_id = get_sub_field('prop_featured_project_image',$post->ID); 
				$img = wp_get_attachment_image_src( $img_id,'full' );
				echo '<div class="entry-thumbnail entry-media">';?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" ><img src="<?php echo $img[0];?>" width="300" height="224" /></a>
				<?php
				echo '</div>';
			}	

		endwhile;
	}	

	?>	
	<header class="entry-header">
		<h4 class="entry-title"><a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a></h4>
	</header><!-- .entry-header -->
	
	<div class="entry-summary entry-body">
		<?php get_template_part('rock-excerpt'); ?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->

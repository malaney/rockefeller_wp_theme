<?php
/**
* The template used for displaying page content in page-homepage.php
 */
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	


		<?php get_template_part( 'content', 'homepage-rotating-images' ); ?>
		
		<section class="region-content">
				<div id="perspectives" class="col col_1">
					<h3 class="section-header">Perspectives</h3>
					<?php

					$perspectives = get_field('homepage_perspectives');

					if($perspectives) {
						$args = array(
							'post_type' => 'perspectives',
							'posts_per_page' => 1,
							'p' => $perspectives[0]->ID
						);

						$the_query = new WP_Query( $args );

						// The Loop
						while ( $the_query->have_posts() ) :
							$the_query->the_post(); 
							get_template_part( 'content', 'homepage-perspectives-project' );
						endwhile;

						
						wp_reset_postdata();
					}	
					?>
				</div>
				<div id="featured-project" class="col col_2">
					<h3 class="section-header">Featured Project</h3>
			
					<?php

						$featured = get_field('homepage_featured_project');

						if($featured) {
							$args = array(
								'post_type' => 'properties',
								'posts_per_page' => 1,
								'p' => $featured[0]->ID
							);

							$the_query = new WP_Query( $args );

							// The Loop
							while ( $the_query->have_posts() ) :
								$the_query->the_post(); 
								get_template_part( 'content', 'homepage-featured-project' );
								
							endwhile;

							
							wp_reset_postdata();
						}	

					?>
				</div>

				<div id="news" class="col col_3">
					<h3 class="section-header">News</h3>
					
					<?php

						$args = array(
							'post_type' => 'post',
							'posts_per_page' => 3
						);

						$the_query = new WP_Query( $args );

						// The Loop
						while ( $the_query->have_posts() ) :
							$the_query->the_post(); 
							get_template_part( 'content', 'homepage-news' );
							
						endwhile;

							
						wp_reset_postdata();

					?>


				</div>	
		</section>	
	
</div><!-- #post-<?php the_ID(); ?> -->

<?php
/**
 * The template for displaying all pages.
 *
 */

get_header(); ?>


	<div class="region-content">
		<div id="primary">
			<div id="content" role="main">
				

				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', 'page' ); ?>

				<?php endwhile; // end of the loop. ?>
				<?php wp_reset_postdata(); ?>
			</div><!-- #content -->
		</div><!-- #primary -->

<?php get_sidebar('page'); ?>
</div>
<?php get_footer(); ?>
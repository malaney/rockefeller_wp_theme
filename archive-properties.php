<?php
/**
 * The template for displaying the properties page
 * Template Name: Properties Page
 *
 */

get_header(); ?>


		<div class="region-top">
		
		<?php 
			$args = array(
				'pagename' => 'properties-page'
			);

			$the_query = new WP_Query( $args );

			// The Loop
			while ( $the_query->have_posts() ) :
				$the_query->the_post(); 

				if( get_field('property_pg_featured_image') ): ?>
					<div class="featured-image">
						<?php
						$att_id = get_field('property_pg_featured_image');
						print wp_get_attachment_image($att_id, 'full');
						?>
					</div>

					<?php
				endif;
			endwhile;

			wp_reset_postdata();

		?>
			
			

		<?php
			// $args = array(
			// 	'hide_empty'    => false,
			// );

			$terms = apply_filters( 'taxonomy-images-get-terms', '', array(
			    'taxonomy' => 'property_categories',
					'having_images' => false,
					'term_args' => array(
						'hide_empty' => 0,
					),
			    ) );

			?>	

			<?php if($terms) { ?>

				<ul class="categories">
				<?php
				foreach( $terms as $tax_term ) {
					if ($tax_term->image_id) {
						echo '<li>' . '<span class="section-header"><a href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '" title="' . sprintf( __( "View all Properties in %s" ), $tax_term->name ) . '" ' . '>' .  $tax_term->name . '</a></span><a href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '" title="' . sprintf( __( "View all Properties in %s" ), $tax_term->name ) . '" ' . '>' . wp_get_attachment_image( $tax_term->image_id, 'thumbnail' ).'</a></li>';
					} else {
						echo '<li>' . '<a href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '" title="' . sprintf( __( "View all Properties in %s" ), $tax_term->name ) . '" ' . '>' . $tax_term->name.'</a></li>';
					}	
				}
			}
			?>
				</ul>
			<?php wp_reset_postdata(); ?>	
		</div>	
	<div class="region-content">
		<div id="primary">
			<div id="content" role="main">

				<?php

					$args3 = array(
						'pagename' => 'properties-page'
					);
					

					$the_query = new WP_Query( $args3 );

					// The Loop
					while ( $the_query->have_posts() ) :
						$the_query->the_post(); 

						if(get_field('properties_pg_subtitle')) {
							echo '<header><h3 class="subtitle">' .get_field('properties_pg_subtitle', $post->ID) . '</h3></header>';
						} ?>

							
						<div class="entry-body entry-content">	
							<?php the_content(); ?>
						</div>
						
					<?php endwhile; ?>

					<?php wp_reset_postdata(); ?>
			</div><!-- #content -->
		</div><!-- #primary -->

<?php get_sidebar('properties-page'); ?>
</div>	

<?php get_footer(); ?>
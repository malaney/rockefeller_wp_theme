<?php 
 /**
 * The template used for specific property's misc details in content-single-properties.php
 */


		$addHTML = false; 
		$addSqFT = false;
		$addLocation = false;
		$addMapURL = false;

	?>


		<?php
		if(get_field('prop_information')) {
			
			while( has_sub_field('prop_information') ):
				if( get_sub_field('prop_square_footage')) {
					$addHTML = true;
					$addSqFT = true;
				}



				if(get_sub_field('prop_location')){
					while( has_sub_field('prop_location') ):
						if(get_sub_field('prop_city')){
							$addHTML = true;
							$addLocation = true;
						}
					endwhile;	
				}


				if(get_sub_field('prop_map_url')) {
					$addHTML = true;
					$addMapURL = true;
				}


			endwhile;
		}

		if($addHTML) { ?>
			<div class="entry-details entry-meta">

			<?php	
			if($addLocation) {
				 if( get_field('prop_information') ): 
				 	while( has_sub_field('prop_information') ): 
				 		if( get_sub_field('prop_location') ): 
				 			while( has_sub_field('prop_location') ):  ?>
				 					<div class="location detail">
										<span class="label">Location:</span>
										<span class="location property-info">	
											<?php echo get_sub_field('prop_city') . ', ' . get_sub_field('prop_state'); ?>
										</span>
									</div>	
				 			<?php 
				 			endwhile;
				 		endif;
				 	endwhile;
				 endif;
			}


			if($addSqFT) {
			  if( get_field('prop_information') ): 
	 			while( has_sub_field('prop_information') ):  ?>
					<div class="size detail">
						<span class="label">Square footage:</span>
						<span class="sqft property-info">	
							<?php echo get_sub_field('prop_square_footage') . ' sq. ft.' ?>
						</span>
					</div>	
	 			<?php 
	 			endwhile;
			 endif;
			}

			if($addMapURL) {
				if( get_field('prop_information') ): 
						while( has_sub_field('prop_information') ):  
							if(get_sub_field('prop_map_url')) : ?>
								<div class="map">
									<a href="<?php echo get_sub_field('prop_map_url')?>" target="_blank">View On Map</a>
								</div>	
						<?php 
							endif;
						endwhile;
				endif;
			}	
		?>

	</div>

	<?php } ?>
<?php
 /**
 * The template used for displaying navigation in single.php
 */
?>

			<div class="navigation pager">
				<div class="prev"><?php previous_post_link('%link') ?></div>
				<div class="next"><?php next_post_link('%link') ?></div> 
			</div>	
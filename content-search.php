<?php
/**
 * The template used for displaying page content in search.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('teaser'); ?>>
	<header class="entry-header">
		<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'rockefeller' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary entry-body">
		 <?php get_template_part('rock-excerpt'); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-summary entry-body">
		<?php get_template_part('rock-excerpt'); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'rockefeller' ), 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->
	<?php endif; ?>


</article><!-- #post-<?php the_ID(); ?> -->

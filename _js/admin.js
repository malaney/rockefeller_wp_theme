

jQuery(document).ready(function() {
// Handler for .ready() called.


		//Agent Form Validation
		jQuery("#post #acf-agent_email input").addClass('email acf-validation-field');
		jQuery("#post #acf-agent_phone input").addClass('acf-phone acf-validation-field');	

			
		jQuery("#post").validate();		

			// set up regex for phone through custom method : http://docs.jquery.com/Plugins/Validation/CustomMethods/phoneUS
			jQuery.validator.addMethod("phoneUS", function(phone_number, element) {
		    phone_number = phone_number.replace(/\s+/g, ""); 
			return this.optional(element) || phone_number.length > 9 && phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
		}, "Please enter a valid phone number");
			
			
		jQuery(".acf-phone").each(function(){
			jQuery(this).rules("add", {
				   phoneUS: true
			});                    
		}); 

		jQuery("#publish").click(function(event){
	        //event.preventDefault();

	        if(jQuery("#post").valid()) {
	           	jQuery('#publish').removeClass('button-primary-disabled');
	           	jQuery(".spinner").removeClass('validation-fail-hide-loader');
	            jQuery(this).submit();
	           
	        } else {
	        	 jQuery(".spinner").addClass('validation-fail-hide-loader');
	        }
   	 	});	

		jQuery(".acf-validation-field").change(function(event){

	        if(jQuery("#post").valid()) {
	            jQuery('#publish').removeAttr("disabled");
	            jQuery('#publish').removeClass('button-primary-disabled');
	           
	           
	        } else {
	        	jQuery('#publish').attr("disabled");
	        }
   	 });	


	// Changing prop cats to radio boxes
	jQuery('form#post').find('#property_categorieschecklist input').each(function() {
		var new_input = jQuery('<input type="radio" />'),
		attrLen = this.attributes.length;
		for (i = 0; i < attrLen; i++) {
		if (this.attributes[i].name != 'type') {
		new_input.attr(this.attributes[i].name.toLowerCase(), this.attributes[i].value);
		}
		}
		jQuery(this).replaceWith(new_input);
	});	
	

});

jQuery(document).ready(function() {
// Handler for .ready() called.


	var pageWidth = jQuery('.caroufredsel_wrapper').height();
	jQuery("#featured-images").carouFredSel({
		scroll		: {
			fx			: "fade",
			duration	: 1500,
			easing		: 'linear',
                        onBefore: function(map) {
                            // Grab whichever slide is in the first (scratch that, second) position.  This has to do with timing
                            var activeSlide = jQuery('#featured-images li:nth-child(2)').attr('id').replace('hpi', 'hpt');
                            // Hide all text slides
                            jQuery('ul#featured-text > li.slide').hide();
                            // Show the corresponding text item
                            jQuery("#" + activeSlide).show();

                        }
		},
		pagination  : {
                    container: "#carousel-pager"
                },
		items		: {
			visible		: 1,
			width		: 980,
			height		: 480
		 }
		}, {
	    
	    wrapper     : {
	        element         : "div",
	        classname       : "carousel_1 carousel"
	    }
	});

	//turning off /on carousel if colorbox is on
	jQuery(document).bind('cbox_complete', function(){
			jQuery('#featured-images').trigger('pause'); 
	});
		
	jQuery(document).bind('cbox_closed', function(){
		jQuery('#featured-images').trigger('resume'); 
	});

	

	// ColorBox 
	jQuery(".youtube").colorbox({iframe:true, innerWidth:725, innerHeight:544, opacity:.5});
	
	jQuery(".prop-imgs").colorbox({rel:'prop-imgs', transition:"none", width:"55%", opacity:.5});


	

	jQuery('.teaser').each(function( index ) {
		var read_more = jQuery(this).find('.entry-body .read-more');
		console.log(read_more);
		//jQuery('.teaser .entry-body p').append(read_more).css('background-color','red');
		jQuery(this).find('.entry-body p').append(read_more);
	});
});

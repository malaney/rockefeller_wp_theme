<?php
/**
 * The template used for properties misc details in content-properties.php
 */
?>

	<dl>
		<dt class="entry-title"><?php the_title(); ?></dt>

		<?php 
		
		$addDDtags = false; 
		$addSqFT = false;
		$addLocation = false;

		if(get_field('prop_information')) {
			
			while( has_sub_field('prop_information') ):
				if( get_sub_field('prop_square_footage')) {
					$addDDtags = true;
					$addSqFT = true;
				}
				if(get_sub_field('prop_location')){
					while( has_sub_field('prop_location') ):
						if(get_sub_field('prop_city')){
							$addDDtags = true;
							$addLocation = true;
						}
					endwhile;	

				}

			endwhile;
		}

		if($addDDtags) {
			echo '<dd class="entry-details">';
	
			if($addLocation) {
				if( get_field('prop_information') ): 
				 	while( has_sub_field('prop_information') ): 
				 		if( get_sub_field('prop_location') ): 
				 			while( has_sub_field('prop_location') ):  ?>
				 					<div class="location detail">
										<span class="label">Location:</span>
										<span class="location property-info">	
											<?php echo get_sub_field('prop_city') . ', ' . get_sub_field('prop_state'); ?>
										</span>
									</div>		
				 			<?php 
				 			endwhile;
				 		endif;
				 	endwhile;
				endif;  
			} 

			if($addSqFT) {
				if( get_field('prop_information') ): 
		 			while( has_sub_field('prop_information') ):  ?>
						<div class="size detail">
							<span class="label">Square footage:</span>
							<span class="sqft property-info">	
								<?php echo get_sub_field('prop_square_footage') . ' sq. ft.' ?>
							</span>
						</div>	
		 			<?php 
		 			endwhile;
				 endif;
			}
		
			echo '</dd>';
		}
	?>	

	</dl>	
<?php
/**
 * The Template for displaying all single property post types.
 */

get_header(); ?>

		<div id="primary">
			<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'single-properties' ); ?>


			<?php endwhile; // end of the loop. ?>

			<?php wp_reset_postdata(); ?>
			</div><!-- #content -->
		</div><!-- #primary -->

<?php get_sidebar('properties'); ?>
<?php get_footer(); ?>
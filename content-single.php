<?php
/**
 * The template used for displaying page content in single.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">

		<?php
		if (has_post_thumbnail()) {
			echo '<span class="entry-thumbnail entry-media">';
			the_post_thumbnail();
			echo '</span>';
		}
		?>

		<h1 class="entry-title"><?php the_title(); ?></h1>

		<div class="entry-meta">
			<div class="group_1"><?php rock_posted_on(); ?></div>

			<div class="group_2">
			<?php
				/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list( __( ', ', 'rockefeller' ) );
				$tags_list = get_the_tag_list( '', __( ', ', 'rockefeller' ) );

				if ( $categories_list ) :
			?>
					<span class="cat-links">
						<?php printf( __( 'Categories: %1$s', 'rockefeller' ), $categories_list ); ?>
					</span>
				<?php if($categories_list && $tags_list) { ?>
					<span class="sep"> | </span>
					<?php } ?>
			<?php endif; // End if categories ?>

			<?php		
			
				if ( $tags_list ) :
					?>
						<span class="tag-links">
							<?php printf( __( '<span>Tags:</span> %1$s', 'rockefeller' ), $tags_list ); ?>
						</span>
			<?php endif; // End if $tags_list ?>	

	
		</div>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content entry-body">
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'rockefeller' ), 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->

	
</article><!-- #post-<?php the_ID(); ?> -->

<?php
/**
 * The template used for displaying page content in index.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('teaser'); ?>>
	<header class="entry-header">
		<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'rockefeller' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<div class="group_1"><?php rock_posted_on(); ?></div>

			<div class="group_2">
			<?php
				/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list( __( ', ', 'rockefeller' ) );
				$tags_list = get_the_tag_list( '', __( ', ', 'rockefeller' ) );

				if ( $categories_list ) :
			?>
					<span class="cat-links">
						<?php printf( __( 'Categories: %1$s', 'rockefeller' ), $categories_list ); ?>
					</span>
				<?php if($categories_list && $tags_list) { ?>
					<span class="sep"> | </span>
					<?php } ?>
			<?php endif; // End if categories ?>

			<?php		
			
				if ( $tags_list ) :
					?>
						<span class="tag-links">
							<?php printf( __( '<span>Tags:</span> %1$s', 'rockefeller' ), $tags_list ); ?>
						</span>
			<?php endif; // End if $tags_list ?>	

	
		</div>

		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary entry-body">
		 <?php get_template_part('rock-excerpt'); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<?php
		if (has_post_thumbnail()) {
			$att_id = get_post_thumbnail_id( $post->ID );
			$img = wp_get_attachment_image_src( $att_id );
			echo '<span class="entry-thumbnail entry-media">';?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" ><img src="<?php echo $img[0];?>" width="200" height="131" /></a>
			<?php
			echo '</span>';
		}
	?>
	<div class="entry-summary entry-body">
		<?php get_template_part('rock-excerpt'); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'rockefeller' ), 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->
	<?php endif; ?>


</article><!-- #post-<?php the_ID(); ?> -->

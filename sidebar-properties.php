<?php
/**
 * The template used for displaying taxonomy-property_categories.php's sidebar
 */
?>
		<div id="secondary" class="widget-area" role="complementary">

			<ul class="sidebar">
				<li class="property-categories categories">

					<h3 class="section-header"><a href="/properties">Properties</a></h2>
					<?php
					

					$args = array(
						'hide_empty'    => false,
					);
					$taxonomy = 'property_categories';
					$tax_terms = get_terms($taxonomy, $args);	 
					
					?>	 

					<ul>	
				
					<?php
					foreach( $tax_terms as $tax_term ) {
						$term2 = has_term( $tax_term->slug, $taxonomy);
						$term = is_tax( $taxonomy, $tax_term->name );
						if($term || $term2) {
							$class = 'class="current_page_item"';
						} else {
							$class = '';
						}
						echo '<li '. $class .'>' . '<a href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '" title="' . sprintf( __( "View all posts in %s" ), $tax_term->name ) . '" ' . '>' . $tax_term->name.'</a></li>';
					}
					?>
					</ul>
				</li>
				<?php
				if('properties' == get_post_type() && is_single()) {

					 $docHTML = false;
					 if( get_field('prop_information') ): 
					 	while( has_sub_field('prop_information') ): 
					 		if( get_sub_field('prop_docs') ): 
					 			$docs = get_sub_field('prop_docs');

					 	
					 			foreach($docs[0] as $d ) {
					 				if($d != ''){
					 					$docHTML = true;
					 				}
					 			}
					 		
					 			if($docHTML) {
						 			echo '<li class="related-docs">';
									echo '<h3 class="section-header">Related Info</h3>';
						 			echo '<ul class="documents">';
						 			
						 			foreach($docs as $doc) {
			
						 				if($doc['prop_doc_title']) {
						 					$title = $doc['prop_doc_title'];
						 				} else {	
						 					$title = $doc['prop_doc']['title'];
						 				}	
										$url = $doc['prop_doc']['url'];
										?>
										<li><a href="<?php echo $url; ?>" target="_blank"><?php echo $title; ?></a></li>
									<?php
									}
						 			echo '</ul>';
						 		}	
					 		endif;
					 	endwhile;
					 endif;


					 if( get_field('prop_information') ): 
					 	while( has_sub_field('prop_information') ): 
					 		if( get_sub_field('prop_video') ): 

						 			$videos = get_sub_field('prop_video');
						 			foreach($videos as $video) {
						 				//var_dump($video);
						 				$url = $video['prop_video_url'];
						 				if($url){
						 				echo '<li class="entry-video video entry-media">';
										echo '<h3 class="section-header">Video</h3>';
						 				$title = $video['prop_video_title'];
										$qs = parse_url($url, PHP_URL_QUERY);
										parse_str($qs, $values);
										?>
										<a class='youtube' href="http://www.youtube.com/embed/<?php echo $values['v'] ?>?rel=0&amp;wmode=transparent" title="<?php echo (get_field('video_title') != '') ? the_field('video_title') : '' ?>">
											<img src="http://img.youtube.com/vi/<?php echo $values['v'];?>/0.jpg" />
										</a>
										<?php } ?>									
									<?php
									}
					 		endif;
					 	endwhile;
					 endif;
				

				} 

			?>


		</ul>			

	</div><!-- #secondary .widget-area -->


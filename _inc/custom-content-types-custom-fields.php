<?php
/***

	SITE Custom Post Types, Custom Taxonomy and Advanced Custom Fields

***/

/*--------------------------------------------------------------------------------------
*
*
*	@desc This function should contain anything that needs to happen on Wordpress init. Custom post types/taxonomy, Advanced Custom Fields, etc.

* 
*-------------------------------------------------------------------------------------*/
if ( ! function_exists( 'rock_custom_init' ) ){
	

	function rock_custom_init() {
		register_post_type('properties', array(	
			'label' => 'Properties',
			'description' => '',
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 6,
			'capability_type' => 'post',
			'hierarchical' => false,
			'rewrite' => array('slug' => 'properties', 'with_front' => false),
			'query_var' => true,
			'has_archive' => true,
			'supports' => array('title','editor','thumbnail'),
			'labels' => array(
			      'name' => 'Properties',
			      'singular_name' => 'Property',
			      'all_items' => 'All Properties',
			      'menu_name' => 'Properties',
			      'add_new' => 'Add New',
			      'add_new_item' => 'Add New Property',
			      'edit' => 'Edit',
			      'edit_item' => 'Edit Property',
			      'new_item' => 'New Property',
			      'view' => 'View Property',
			      'view_item' => 'View Property',
			      'search_items' => 'Search Properties',
			      'not_found' => 'No Properties Found',
			      'not_found_in_trash' => 'No Properties found in Trash',
			      'parent' => 'Parent Property',
		),) );

		register_post_type('perspectives', array(	
			'label' => 'Perspectives',
			'description' => '',
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 7,
			'capability_type' => 'post',
			'hierarchical' => false,
			'rewrite' => array('slug' => 'perspectives', 'with_front' => false),
			'query_var' => true,
			'has_archive' => true,
			'supports' => array('title'),
			'labels' => array(
			      'name' => 'Perspectives',
			      'singular_name' => 'Perspective',
			      'all_items' => 'All Perspectives',
			      'menu_name' => 'Perspectives',
			      'add_new' => 'Add New',
			      'add_new_item' => 'Add New Perspective',
			      'edit' => 'Edit',
			      'edit_item' => 'Edit Perspective',
			      'new_item' => 'New Perspective',
			      'view' => 'View Perspective',
			      'view_item' => 'View Perspective',
			      'search_items' => 'Search Perspectives',
			      'not_found' => 'No Perspectives Found',
			      'not_found_in_trash' => 'No Perspectives found in Trash',
			      'parent' => 'Parent Perspective',
		),) );


		register_post_type('agents', array(	
			'label' => 'Agents',
			'description' => '',
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 8,
			'capability_type' => 'post',
			'hierarchical' => false,
			'rewrite' => array('slug' => 'agents', 'with_front' => false),
			'query_var' => true,
			'has_archive' => true,
			'supports' => array('title'),
			'labels' => array(
			      'name' => 'Agents',
			      'singular_name' => 'Agent',
			      'all_items' => 'All Agents',
			      'menu_name' => 'Agents',
			      'add_new' => 'Add New',
			      'add_new_item' => 'Add New Agent',
			      'edit' => 'Edit',
			      'edit_item' => 'Edit Agent',
			      'new_item' => 'New Agent',
			      'view' => 'View Property',
			      'view_item' => 'View Agent',
			      'search_items' => 'Search Agents',
			      'not_found' => 'No Agents Found',
			      'not_found_in_trash' => 'No Agents found in Trash',
			      'parent' => 'Parent Agent',
		),) );
	
	


		$args = array(
		    'label'                         => 'Categories',
		    'labels'                => array(
		        'name'              => __( 'Property Categories' ),
		        'singular_name'     => __( 'Property Category' )
		        ),
		    'public'                        => true,
		    'show_admin_column' => true,
		    'hierarchical'                  => true,
		    'show_ui'                       => true,
		    'show_in_nav_menus'             => true,
		    'args'                          => array( 'orderby' => 'term_order' ),
		    'rewrite'                       => array( 'slug' => 'properties', 'with_front' => false ),
		    'query_var'                     => true
		);

			register_taxonomy( 'property_categories', array('properties'), $args );

	}
	
	

	add_action( 'init', 'rock_custom_init' );
} 


/**
 * Activate Add-ons
 * Here you can enter your activation codes to unlock Add-ons to use in your theme. 
 * Since all activation codes are multi-site licenses, you are allowed to include your key in premium themes.
 */ 

function my_acf_settings( $options ) {
    // activate add-ons
    $options['activation_codes']['repeater'] = 'QJF7-L4IX-UCNP-RF2W';
    //$options['activation_codes']['options_page'] = 'XXXX-XXXX-XXXX-XXXX';
    $options['activation_codes']['gallery'] = 'GF72-8ME6-JS15-3PZC';
    
    // setup other options (http://www.advancedcustomfields.com/docs/filters/acf_settings/)
    
    return $options;
    
}
add_filter('acf_settings', 'my_acf_settings');

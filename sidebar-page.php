<?php		
 /**
 * The template used for displaying sidebar 3 (Basic Page sidebar)
 */


 		if (!is_page(array('terms','privacy-policy'))) {
		if ( is_active_sidebar( 'sidebar-3' ) ) : ?>
			<div id="secondary" class="widget-area" role="complementary">
				<ul class="sidebar">
					<?php dynamic_sidebar( 'sidebar-3' ); ?>
				</ul>
			</div><!-- #secondary .widget-area -->
				
		<?php endif; 
	   }
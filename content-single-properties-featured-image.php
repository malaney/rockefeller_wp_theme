<?php
/**
 * The template used for specific property's featured image in content-single-properties.php
 */


		if (has_post_thumbnail()) {
			echo '<div class="featured image entry-thumbnail entry-media">';
			the_post_thumbnail('property-featured');
			echo '</div>';
		} ?>
<?php
/**
 * Template Name: Homepage Template
 * The template used for displaying the homepage
 */

get_header(); ?>

		<div id="primary">
			<div id="content" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'homepage' ); ?>


				<?php endwhile; // end of the loop. ?>

				<?php wp_reset_postdata(); ?>
			</div><!-- #content -->
		</div><!-- #primary -->

<?php get_footer(); ?>
<?php
/**
* The template used for displaying page content in single-properties.php
*/
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>


		<div class="region-content">


			<header class="entry-header">
				

				<?php get_template_part('content','single-properties-featured-image'); ?>

				<?php get_template_part('content','single-properties-image-gallery'); ?>
			
				<h1 class="entry-title"><?php the_title(); ?></h1>

				<?php get_template_part('content','single-properties-misc-details'); ?>

			</header><!-- .entry-header -->

			

			<div class="entry-content entry-body">
				<?php the_content(); ?>
			</div>

			<?php get_template_part('content','single-agent-info'); ?>
			

				

		</div>
		

	<footer class="entry-meta pager">

			<?php

				
				$taxonomy = 'property_categories';
				$tax_terms = get_terms($taxonomy);

				foreach( $tax_terms as $tax_term ) {
					$tax_name = has_term( $tax_term->slug, $taxonomy);
					if($tax_name) { ?>

						<span class="cat-link prev">
							<?php echo '<a href="/properties/' . $tax_term->slug .'">Back to "' . $tax_term->name . '"</a>'; ?> 
						</span>
					<?php
					}
				
				}	?> 
	</footer><!-- .entry-meta -->
</article><!-- #post-<?php the_ID(); ?> -->

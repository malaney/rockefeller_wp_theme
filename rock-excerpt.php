<?php 
 /**
 * The template used for custom excerpt in various places
 */
	

	$len = 20;

	if(!is_front_page()) {
		$len = 80;
	}
	$content = '';
	$excerpt = '';
	$content = wp_trim_words( get_the_content(), $len, '' ); 
	if(get_field('acf_excerpt')){
		$excerpt = get_field('acf_excerpt');
	}

	$read_more = '';

	if(get_post_type() != 'perspectives') {
		$read_more = '<span class="read-more"> <a href="'. get_permalink() . '">Read More</a></span>';
	}
		

	if( $excerpt ) {
		echo $excerpt . $read_more;
	} else {
		echo wpautop( $content) . $read_more;
	}
	
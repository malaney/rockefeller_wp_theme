<?php
/**
 * The Sidebar containing the default sidebar for news and posts 
 */
?>
		
		<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
			<div id="secondary" class="widget-area" role="complementary">
				<ul class="sidebar">
					<?php dynamic_sidebar( 'sidebar-1' ); ?>
				</ul>
			</div><!-- #secondary .widget-area -->
				
		<?php endif; ?>



		


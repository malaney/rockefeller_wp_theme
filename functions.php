<?php


/**************************
  Include Custom Post Types, 
  Custom Taxonomy,
  Advanced Custom Fields 
*************************/
require_once('_inc/custom-content-types-custom-fields.php');


/**************************
  Adding style sheets 
*************************/
if ( ! function_exists( 'rock_init' ) ) {
 function rock_init() {
  if (!is_admin()) {
 
   wp_enqueue_style( 'global-style', get_stylesheet_directory_uri() . '/_css/global.css', false);
   wp_enqueue_style( 'master-style', get_stylesheet_directory_uri() . '/_css/master.css', false);
   wp_enqueue_style('colorbox-style', get_stylesheet_directory_uri() . '/_css/colorbox.css');
  } else {
    wp_enqueue_style( 'admin', get_stylesheet_directory_uri() . '/_css/admin.css', false);
  }
 }
} // rock_init
add_action('init', 'rock_init');
 

 /**************************
  Adding js
*************************/

if ( ! function_exists( 'rock_add_scripts' ) ) {
 function rock_add_scripts() {
 
  wp_enqueue_script( 'jquery' ); 
  wp_enqueue_script('site', get_stylesheet_directory_uri() . '/_js/site.js', array('jquery-ui-dialog'), '1.0' );
  wp_enqueue_script('carousel', get_stylesheet_directory_uri() . '/_js/jquery.carouFredSel-5.6.1.js', '1.0' );
  wp_enqueue_script('colorbox', get_stylesheet_directory_uri() . '/_js/jquery.colorbox-min.js', '1.0');
 }    
} // rock_add_scripts
add_action('wp_enqueue_scripts', 'rock_add_scripts');


if ( ! function_exists( 'rock_add_admin_scripts' ) ) {
  function rock_add_admin_scripts($pagehook) { 
     global $post_type;
     $pages = array( 'post.php', 'post-new.php' );
      if ( in_array( $pagehook, $pages ) && $post_type == 'agents' || $post_type == 'properties' ) {  
        wp_enqueue_script('validation', get_stylesheet_directory_uri() . '/_js/jquery.validate.min.js', '1.0');
        wp_enqueue_script('admin', get_stylesheet_directory_uri() . '/_js/admin.js', '1.0' );
      }
  }
} // rock_add_admin_scripts
add_action( 'admin_enqueue_scripts', 'rock_add_admin_scripts' );


/**************************
  Adding Widgets
*************************/
 
function rock_add_header_widgets() {
 
 register_sidebar( array(
  'name' => __( 'News Sidebar', 'rockefeller' ),
  'id' => 'sidebar-1',
  'before_widget' => '<li id="%1$s" class="widget %2$s">',
  'after_widget' => "</li>",
  'before_title' => '<header><h3 class="widget-title section-header">',
  'after_title' => '</h3></header>',
 ) );

 register_sidebar( array(
  'name' => __( 'Properties Page Sidebar', 'rockefeller' ),
  'id' => 'sidebar-2',
  'before_widget' => '<li id="%1$s" class="widget %2$s">',
  'after_widget' => "</li>",
  'before_title' => '<header><h3 class="widget-title section-header">',
  'after_title' => '</h3></header>',
 ) );

  register_sidebar( array(
    'name' => __( 'Basic Page Sidebar', 'rockefeller' ),
    'id' => 'sidebar-3',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => "</li>",
    'before_title' => '<header><h3 class="widget-title section-header">',
    'after_title' => '</h3></header>',
   ) );
 
} // add_header_widgets
add_action( 'init', 'rock_add_header_widgets' );

/**************************
  Adding featured images functionality
*************************/

function rock_check_thumbnail_support() {  
 if (!current_theme_supports('post-thumbnails')) {  
   add_theme_support( 'post-thumbnails', array('post','properties'));  
 }  
}  
add_action('after_setup_theme','rock_check_thumbnail_support',99);  



/**************************
  Adding Menus 
*************************/
function rock_add_menus() {

  register_nav_menus( array(
    'primary' => __('Header Nav', 'rockefeller'),
    'secondary' => __('Footer Nav', 'rockefeller'),
  ));

}

add_action('after_setup_theme','rock_add_menus');


/**************************
  Adding Body classes depending on conditions
*************************/
function rock_add_body_classes() {
  $home_pg_class = (!is_front_page()) ? 'not-front' : 'front';
  $page_class = (('properties' == get_post_type()) && is_archive()) || (is_search())? 'page' : '';
  $prop_class = is_tax( 'property_categories' ) || ('properties' == get_post_type()) ? 'properties' : '';
  
  /* has-sidebar logic */
  $sidebar1 = is_active_sidebar( 'sidebar-1');
  $sidebar2 = is_active_sidebar( 'sidebar-2');
  $sidebar3 = is_active_sidebar( 'sidebar-3');

  $sidebar = false;

  if($sidebar1 && is_home() || is_single() || is_search() || is_category() || is_404() || is_archive() && 'post' == get_post_type()){
    $sidebar = true;
  } elseif($sidebar2 && is_archive('properties')) {
    $sidebar = true;
  } elseif('page' == get_post_type() && !is_page(array('terms','privacy-policy')) && !is_front_page() && !is_search() ) {
    if($sidebar2 && $sidebar3) {
      $sidebar = true;
    } elseif ($sidebar3) {
       $sidebar = true;
    } 
  }elseif(is_single() || is_tax( 'property_categories' )) {
    $sidebar = true;
  }

  if ($sidebar) {
    $sidebar_class = 'has-sidebar';
  } 
   
  $classes = $home_pg_class . ' ' . $page_class . ' ' . $prop_class . ' ' . $sidebar_class; 
  return $classes;
}

/**************************
  Modifying posted on date
*************************/

if ( ! function_exists( 'rock_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 * Create your own rock_posted_on to override in a child theme
 */
function rock_posted_on() {
  printf( __( '<span class="sep">Posted on </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s" pubdate>%4$s</time></a><span class="byline"> <span class="sep"> by </span> <span class="author vcard"><a class="url fn n" href="%5$s" title="%6$s" rel="author">%7$s</a></span></span>', 'rockefeller' ),
    esc_url( get_permalink() ),
    esc_attr( get_the_time() ),
    esc_attr( get_the_date( 'c' ) ),
    esc_html( get_the_date() ),
    esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
    esc_attr( sprintf( __( 'View all posts by %s', 'rockefeller' ), get_the_author() ) ),
    esc_html( get_the_author() )
  );
}
endif;



/*******************************************
  Modifying homepage features
*******************************************/

function rock_remove_homepage_features(){
    $page = get_page_by_title('homepage');
    if(get_the_ID() == $page->ID ) {
        remove_post_type_support( 'page', 'editor' );
        //remove_post_type_support( 'page', 'thumbnail' );
        
    } // end if
} 
add_action( 'add_meta_boxes', 'rock_remove_homepage_features' );


/*******************************************
  Modifying pages features
*******************************************/

function rock_remove_page_features(){
    remove_post_type_support( 'page', 'excerpt' );
    remove_post_type_support( 'page', 'custom-fields' );
    remove_post_type_support( 'page', 'trackbacks' );
    remove_post_type_support( 'page', 'comments' );
} 
add_action( 'init', 'rock_remove_page_features' );

/*******************************************
  Modifying pages features
*******************************************/

function rock_remove_post_features(){
    remove_post_type_support( 'post', 'excerpt' );
    remove_post_type_support( 'post', 'custom-fields' );
    remove_post_type_support( 'post', 'trackbacks' );
    remove_post_type_support( 'post', 'comments' );
} 
add_action( 'init', 'rock_remove_post_features' );


/*******************************************
  Adding custom image sizes
*******************************************/
if ( function_exists( 'add_image_size' ) ) { 

  add_image_size( 'property-large-thumb', 292, 192 ); 
  add_image_size( 'property-small-thumb', 140, 92 ); 
  add_image_size( 'homepage-featured-thumb', 300, 224 ); 
  add_image_size( 'property-featured', 730,480 );
 add_image_size( 'post-thumbs', 200, 175, true );
}

 
add_filter('image_size_names_choose', 'my_image_sizes');
function my_image_sizes($sizes) {
        $addsizes = array(
            "property-small-thumb" => __( "Property Small Thumb"),
            "property-large-thumb" => __( "Property Large Thumb"),
            "homepage-featured-thumb" => __( "Homepage Featured Thumb"),
            "property-featured" => __( "Featured"), // full
            "post-thumbs" => __("post-thumbs")    
                );
        $newsizes = array_merge($sizes, $addsizes);
        return $newsizes;
}

/********************************************************
  Adding colorbox class (prop-imgs) for single pg properties
*******************************************/

function rock_add_class_attachment_link($html){
    if('properties' == get_post_type() && is_single()) {
      $postid = get_the_ID();
      $html = str_replace('<a','<a class="prop-imgs"',$html);
    }
    return $html;
}
add_filter('wp_get_attachment_link','rock_add_class_attachment_link',10,1);


/********************************************
 Filtering a Class in Navigation Menu Item
 *******************************************/

function rock_add_nav_item_class($classes, $item){
     if(has_nav_menu('secondary') ) {
        $classes[] = mb_strtolower($item->title);
     }
     return $classes;
}
add_filter('nav_menu_css_class' , 'rock_add_nav_item_class' , 10 , 2);



/********************************************
 Modifying properties query (posts per page) on the tax page
 *******************************************/

function rock_properties_taxpg_query( $query ) {
  if ( is_tax( 'property_categories' )  ) {
    $query->set( 'posts_per_page', 3 );
  }  
  return $query;
}
add_action( 'pre_get_posts', 'rock_properties_taxpg_query' );



/********************************************
  Adding properties category filter in admin
*******************************************/
function rock_add_taxonomy_filters() {
  global $typenow;
 
  // an array of all the taxonomyies you want to display. Use the taxonomy name or slug
  $taxonomies = array('property_categories');
 
  // must set this to the post type you want the filter(s) displayed on
  if( $typenow == 'properties' ){
 
    foreach ($taxonomies as $tax_slug) {
      $tax_obj = get_taxonomy($tax_slug);
      $tax_name = $tax_obj->labels->name;
      $terms = get_terms($tax_slug);
      if(count($terms) > 0) {
        echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
        echo "<option value=''>view all " . strtolower($tax_name) ."</option>";
        foreach ($terms as $term) { 
          echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>'; 
        }
        echo "</select>";
      }
    }
  }
}
add_action( 'restrict_manage_posts', 'rock_add_taxonomy_filters' );


/*
Set up Property Custom Tax to use same slug as Property CPT
Took from here:
http://someweblog.com/wordpress-custom-taxonomy-with-same-slug-as-custom-post-type/
*/
function taxonomy_slug_rewrite($wp_rewrite) {
    $rules = array();
 
    // get all custom taxonomies
    $taxonomies = get_taxonomies(array('_builtin' => false), 'objects');
 
    // get all custom post types
    $post_types = get_post_types(array(
        'public' => true,
        '_builtin' => false),
    'names');
 
    foreach ($post_types as $post_type) {
        foreach ($taxonomies as $taxonomy) {
 
            // check if taxonomy is registered for this custom type
            if ($taxonomy->object_type[0] == $post_type) {
 
                // get all categories (terms)
                $categories = get_categories(array(
                    'type' => $post_type,
                    'taxonomy' => $taxonomy->name,
                    'hide_empty' => 0
                ));
 
                // make rules
                foreach ($categories as $category) {
                  $rules[$post_type . '/' . $category->slug . '/?$'] =
                    'index.php?' . $category->taxonomy . '=' . $category->slug;
                }
            }
        }
    }
 
    // merge with global rules
    $wp_rewrite->rules = $rules + $wp_rewrite->rules;
}
add_filter('generate_rewrite_rules', 'taxonomy_slug_rewrite');

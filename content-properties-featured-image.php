<?php
/**
 * The template used for properties featured image in content-properties.php
 */

?>
	
	<ul class="entry-images entry-thumbnail entry-media">
		<?php
			if (has_post_thumbnail()) { ?>
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" ><?php the_post_thumbnail('property-large-thumb'); ?></a>
		<?php } else {
			echo '<a href="'. get_permalink() .'" title="' . get_the_title() .'" ><img src="'. get_template_directory_uri(). '/_images/placeholder.jpg" width="292" /></a>';
			}?>
	</ul>	
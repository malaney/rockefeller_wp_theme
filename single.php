<?php
/**
 * The Template for displaying all single posts.
 */

get_header(); ?>

	<div class="region-content">
		<div id="primary">
			<div id="content" role="main">

				<div class="region-content">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'single' ); ?>


			
			<?php endwhile; // end of the loop. ?>

			<?php wp_reset_postdata(); ?>

			<?php get_template_part( 'nav', 'links' ); ?>


				</div>

			</div><!-- #content -->
		</div><!-- #primary -->

<?php get_sidebar(); ?>
</div>	
<?php get_footer(); ?>
 <?php
/**
 * The template used for displaying page content in page.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post teaser'); ?>>

	<div id="video" class="entry-thumbnail entry-media entry-video">
		<?php
		if(get_field('perspectives_video_url')) {

			$url = get_field('perspectives_video_url'); 

			$qs = parse_url($url, PHP_URL_QUERY);
			parse_str($qs, $values);
			?>
	
			<a class="youtube" href="http://www.youtube.com/embed/<?php echo $values['v'] ?>?rel=0&amp;wmode=transparent" title="<?php the_title();?>">
				<img src="http://img.youtube.com/vi/<?php echo $values['v'];?>/0.jpg" width="300" height="186" />
			</a>
		<?php  } ?>
	</div><!-- .entry-video -->
	<header class="entry-header">
		<h4 class="entry-title"><a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a></h4>
	</header><!-- .entry-header -->
	<div class="entry-excerpt entry-body">
		<?php get_template_part('rock-excerpt'); ?>
		<span class="watch-video read-more"><a class="youtube" href="http://www.youtube.com/embed/<?php echo $values['v'] ?>?rel=0&amp;wmode=transparent" title="<?php the_title();?>">Watch</a></span>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->

 <?php
/**
 * The template used for displaying news content on the homepage
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('teaser'); ?>>

		<header class="entry-header">
			<h4 class="entry-title">
				<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'rockefeller' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
			</h4>
		</header><!-- .entry-header -->

	<div class="entry-body entry-excerpt">
		
		<?php get_template_part('rock-excerpt'); ?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
